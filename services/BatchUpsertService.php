<?php

namespace services;

use Throwable;
use Yii;
use yii\db\Exception;

/**
 * Class BatchUpsertService
 * Service for batch insert or update rows: if row duplicating by some unique indexes then it will be overwritten
 *
 * @package common\services
 */
class BatchUpsertService
{
    /**
     * Name of the target table
     *
     * @var string
     */
    private string $tableName;

    /**
     * Count of rows that will be used for one request
     *
     * @var int
     */
    private int $batchSize = 1000;

    /**
     * Target rows like: [ ['column' => 'value'] ]
     *
     * @var array
     */
    private array $rows = [];

    /**
     * Total value of processed rows as result of all requests
     *
     * @var int
     */
    private int $countProcessed = 0;

    /**
     * List of fields that need to be excluded in case of duplicate rows.
     * For example `created_at` is not necessary field for update
     *
     * @var string[]
     */
    private array $excludeUpdateFields = ['created_at'];

    /**
     * Total list of errors
     *
     * @var array
     */
    private array $errors = [];

    /** @var bool  */
    private bool $showProgressBar = false;

    private int $totalCount = 0;

    private int $countSliced = 0;

    /**
     * Total count of processed rows
     *
     * @return int
     */
    public function getCountProcessed(): int
    {
        return $this->countProcessed;
    }

    /**
     * Set target table for `upsert` request
     *
     * @param string $tableName
     *
     * @return $this
     */
    public function setTable(string $tableName): self
    {
        $this->tableName = $tableName;

        return $this;
    }

    /**
     * Set batch size for per request
     * Its necessary in case of huge data
     *
     * @param int $val
     *
     * @return $this
     */
    public function setBatchSize(int $val): self
    {
        $this->batchSize = $val;

        return $this;
    }

    /**
     * @param array $rowsForBatchInsert
     *
     * @return $this
     */
    public function setRows(array &$rowsForBatchInsert): self
    {
        $this->rows = $rowsForBatchInsert;
        $this->totalCount = count($rowsForBatchInsert);

        return $this;
    }

    /**
     * @param array $val
     *
     * @return $this
     */
    public function setExcludeUpdateFields(array $val): self
    {
        $this->excludeUpdateFields = $val;

        return $this;
    }

    /**
     * @return array|string[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    public function setShowProgressBar(bool $val): self
    {
        $this->showProgressBar = $val;
        return $this;
    }

    /**
     * Divides input self::$rows by self::$batchSize then make request for `upsert` (insert on duplicate update)
     *
     * @return $this
     */
    public function batchUpsert(): self
    {
        try {

            // slice by batch count
            $sliced = array_splice($this->rows, 0, $this->batchSize);
            $this->countSliced += count($sliced);

            // if empty sliced batch then stop
            if (empty($sliced)) {
                return $this;
            }

            // prepare sql for batch insert
            $fields = array_keys(current($sliced));
            $table = $this->tableName;
            $db = Yii::$app->db;
            $sql = $db->queryBuilder->batchInsert($table, $fields, $sliced);

            // update fields without excluded fields
            $fieldsToUpdate = array_filter($fields, function ($val) {
                return in_array($val, $this->excludeUpdateFields, true) === false;
            });
            $updateValuesOnDuplicate = implode(', ',
                array_map(static function ($field) {
                    return $field . ' = VALUES(' . $field . ')';
                }, $fieldsToUpdate)
            );

            // execute prepared sql for batch insert
            $this->countProcessed += $db
                ->createCommand("{$sql} ON DUPLICATE KEY UPDATE {$updateValuesOnDuplicate}")
                ->execute();

            // if input rows not empty then slice and batch insert it again
            if (!empty($this->rows)) {
                return $this->batchUpsert();
            }
        } catch (Throwable $e) {
            $this->errors [] = substr($e->getMessage(), 0, 255);

            return $this->batchUpsert();
        }

        return $this;
    }
}
